Meteor.startup(function () {
	var	smtpExist = SMTP.findOne({});

	if (smtpExist) {
		var email 	= smtpExist.email
		var smtp 	= smtpExist.smtp
		var port		= smtpExist.port

		var decryptPwd = CryptoJS.AES.decrypt( smtpExist.password, "");
		var password	= decryptPwd.toString(CryptoJS.enc.Utf8)

		email = email.replace("@", "%40");

	   process.env.MAIL_URL="smtp://"+email+":"+password+"@"+smtp+":"+port;
	}
});