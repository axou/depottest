Meteor.publish('categories', function() {
  return Categories.find();
});

Meteor.publish('sites', function() {
  return Sites.find();
});

Meteor.publish('images', function() {
  return Images.find();
});

Meteor.publish('users', function() {
  return Meteor.users.find();
});

Meteor.publish('qualities', function() {
  return Qualities.find();
});

Meteor.publish('annonces', function() {
  return Annonces.find();
});

Meteor.publish('plublication', function() {
  return Annonces.find({status: {$in : ["Publiée", "Réservée"]}});
});

Meteor.publish('newsletters', function() {
  return Newsletters.find();
});