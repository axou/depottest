var createUser = false

if(typeof Meteor.users.find({'profile.isAdmin': true}).count() == "undefined" ) {
    createUser = true
}else{
    if(Meteor.users.find({'profile.isAdmin': true}).count() === 0) {
       createUser = true
    }
}

if( createUser ){
    Accounts.createUser({
        username:   'Axelle.Jurin',
        password:   'P@ssword',
        email:      'jurin.axelle@gmail.com',
        profile:    {
            firstname: 'Axelle',
            lastname: 'Jurin',
            isAdmin : true,
            site: 'none'
        }
    });
    Accounts.createUser({
        username:   'Lucile.Grau',
        password:   'P@ssword',
        email:      'lucilegrau@gmail.com',
        profile:    {
            firstname: 'Lucile',
            lastname: 'Grau',
            isAdmin : true,
            site: 'none'
        }
    });
}

if( Qualities.find().count() === 0 ){
    Qualities.insert({
        label: 'Produit neuf et sous garantie',
        stars: 3
    });
    Qualities.insert({
        label: 'Produit utilisé mais toujours garantie',
        stars: 2
    });
    Qualities.insert({
        label: 'Produit d\'occasion hors garantie',
        stars: 1
    });
}

if ( Categories.find().count() === 0 ){
    Categories.insert({
        label: 'Informatique'
    });
    Categories.insert({
        label: 'Logistique'
    });  
    Categories.insert({
        label: 'Fourniture'
    });  
    Categories.insert({
        label: 'Ameublement'
    });  
}

if ( Sites.find().count() === 0 ){
    Sites.insert({
        name:       'Vaulx-Milieu',
        address:    '80, rue Condorcet, Bâtiment Le Dauphine',
        zipCode:    '38090',
        city:       'Vaulx-Milieu',
        longitude:  5.174206799999979, 
        latitude:   45.6243868 
    });
    Sites.insert({
        name:       'Saint-Quentin 1',
        address:    '19, rue des Garines',
        zipCode:    '38070',
        city:       'Saint-Quentin-Fallavier',
        longitude:  5.12378769999998, 
        latitude:   45.65480439999999
    });
    Sites.insert({
        name:       'Saint-Quentin 2',
        address:    '45, rue Santoyon',
        zipCode:    '38070',
        city:       'Saint-Quentin-Fallavier',
        longitude:  5.112448900000004, 
        latitude:   45.6634106
    });
    Sites.insert({
        name:       'Saint-Quentin 3',
        address:    '19, rue des Garines',
        zipCode:    '38070',
        city:       'Saint-Quentin-Fallavier',
        longitude:  5.12378769999998, 
        latitude:   45.65480439999999
    });
    Sites.insert({
        name:       'La Verpillière',
        address:    'Chemin de Malatrait',
        zipCode:    '38290',
        city:       'La Verpillièr',
        longitude:  5.1379799999999705, 
        latitude:   45.646004
    });
}

if( SMTP.find().count() === 0 ) {
    SMTP.insert({
        email: "jurin.axelle@gmail.com",
        password: "U2FsdGVkX19Kmv6uIpvQIeZ0qST24Exhmk/TU0DXS4c=",
        smtp: "smtp.gmail.com",
        port: "587"
    });
}
