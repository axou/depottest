//------------ CREATION DES TACHES ------------

// SEND NEWSLETTERS MONTHLY
// var scheduleM = later.parse.recur().on(00).second(); // test toutes les minutes à 0 secondes
var scheduleM = later.parse.recur().on(1).month();
var monthlyEmailer = new ScheduledTask(scheduleM, function(){
	Newsletters.find({frequency:"monthly"}).forEach( function(newsletter){
		Meteor.call('send_newsletters', newsletter);
		// console.log("task monthly");
	});
});

// SEND NEWSLETTERS WEEKLY
// var scheduleW = later.parse.recur().on(20).second(); // test toutes les minutes à 20 secondes
var scheduleW = later.parse.recur().on(1).onWeekday();
var weeklyEmailer = new ScheduledTask(scheduleW, function(){
	Newsletters.find({frequency:"weekly"}).forEach( function(newsletter){
		Meteor.call('send_newsletters', newsletter);
		// console.log("task weekly");
	});
});

// SEND NEWSLETTERS DAILY
// var scheduleD = later.parse.recur().on(40).second(); // test toutes les minutes à 40 secondes
var scheduleD = later.parse.recur().on(9).hour();
var dailyEmailer = new ScheduledTask(scheduleD, function(){
	Newsletters.find({frequency:"daily"}).forEach( function(newsletter){
		Meteor.call('send_newsletters', newsletter);
		// console.log("task daily");
	});
});

// QUOTIDIEN TASK
var time = later.parse.recur().on(40).second(); // test toutes les minutes à 40 secondes
// var scheduleD = later.parse.recur().on(10).hour();
var quotidienTask = new ScheduledTask(time, function(){
	Meteor.users.find().forEach(function(user){
		Meteor.call('send_alertExpire', user._id);
		// console.log("task expire");
	});
});

//------------ LANCEMENT DES TACHES ------------
Meteor.startup(function () {
	monthlyEmailer.start();
	weeklyEmailer.start();
	dailyEmailer.start();
	quotidienTask.start();
});