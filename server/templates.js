SSR.compileTemplate('emails_layout', Assets.getText('emails_layout.html'));
SSR.compileTemplate('annonces_template', Assets.getText('annonces_template.html'));

Template.emails_layout.helpers({
  lienLogo: function(){
    return Meteor.absoluteUrl('rhenus-logo.jpg');
  }
});

Template.annonces_template.helpers({
  lienImage: function(){
    if (this.documents !== undefined){
      var img = Images.findOne(this.documents[0]);
      return Meteor.absoluteUrl(img.url);
    }
  },
  lienAnnonce: function(){
    return Meteor.absoluteUrl("Annonce/"+this._id);
  }
});