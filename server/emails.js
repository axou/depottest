// -------------- RESET PASSWORD ---------------
Accounts.emailTemplates.siteName = "Rhenus Logistiques";
Accounts.emailTemplates.from = "Rhenus Logistiques <services@fr.rhenus.com>";
Accounts.emailTemplates.resetPassword = {
  subject(user) {
    return "Initialisation de votre mot de passe";
  },
  html(user, url) {
    var content = `Veuilliez cliquer sur le lien suivant pour réinitialiser votre mot de passe : `;
    var html  = SSR.render('emails_layout', {content: content, footer:url});
    return html;
  }
};


// -------------- METHOD SEND EMAIL ---------------
Meteor.methods({
  send_passwordLink: function (email) {
    var user = Meteor.users.findOne({"emails.address": email});

    if (user) {
      Accounts.urls.resetPassword = function (token) {
        var when = new Date();

        var tokenRecord = {
          token: token,
          email: email,
          when: when
        };

        Meteor.users.update(user._id, {$set: {
          "services.password.reset": tokenRecord
        }});

        return Meteor.absoluteUrl(`resetPassword/${token}`);
      };

      Accounts.sendResetPasswordEmail (user._id);
    }
  },
  send_newsletters: function(newsletter){
    var annonces = [];

    var countAnnonce =  Annonces.find({
                          category: {$in : newsletter.favoriteCategories},
                          status: "Publiée"
                        }, {limit: 6}).forEach(function(annonce){
                          if (typeof newsletter.lastSend != "undefined"){
                            var a = moment(annonces.publication, 'DD-MM-YYYY');
                            var b = moment(newsletter.lastSend, 'DD-MM-YYYY');

                            if ( a.diff(b)>0 ){
                              annonces.push(annonce);
                            };
                          }else{
                            annonces.push(annonce);
                          }
                        });

    if (countAnnonce>0){
      var content =  ``;

      var html = SSR.render('emails_layout', {content: content, annonces: annonces});

      Email.send({
        to:       newsletter.emailUser,
        from:     "services@fr.rhenus.com",
        subject:  "Nos dernières annonces",
        html:     html
      });

      Newsletters.update(
        {_id: newsletter._id},
        {$set:
          {
            "lastSend" : moment().format('DD-MM-YYYY')
          }
        }
      );

      console.log("Newsletter envoyé");
    }
  },
  send_alertExpire: function(idUser){
    var annonces = [];
    var timeExpire = moment().add(14, "days").format('DD-MM-YYYY');
    var annonce = Annonces.find({ user: idUser, status:"Publiée", deadline: timeExpire});
    var nbA = Annonces.find({user: idUser, status:"Publiée", deadline: timeExpire}).count();

    var user = Meteor.users.findOne({_id: idUser});
    var content ='';

    if ( nbA!=0 ){
      if ( nbA==1 ){
        annonces.push( annonce );
      }else{
        annonces = annonce
      }

      if ( nbA > 1){
        content = `Ces annonces arrivent bientôt à expiration.
                Vous avez 14 jours pour les mettres à jour sinon elles ne seront plus visible.
                Après ce délai vous pourrez les retrouver dans les annonces expirées de votre espace personnel`;
      }else{
        content = `Cette annonce arrive bientôt à expiration.
                Vous avez 14 jours pour la mettre à jour sinon elle ne sera plus visible.
                Après ce délai vous pourrez le retrouver dans les annonces expirées de votre espace personnel`;
      }

      html = SSR.render('emails_layout', {content: content, annonces: annonces});

      Email.send({
        to:       user.emails[0].address,
        from:     "services@fr.rhenus.com",
        subject:  "Annonce(s) bientôt expirée(s)",
        html:     html
      });

    }
  },
  send_emailReservation: function (id, userId, number){
    var user = Meteor.users.findOne({_id:userId});
    var annonce = Annonces.findOne({_id:id});
    var annonces = [];
    annonces.push( annonce );
    var owner = Meteor.users.findOne({ _id: annonce.user });
    var content = ""
    var footer = ""


    if (user){
      var content = `Vous avez réservé ${number} unité(s) de l'annonce suivante : `;
      var html = SSR.render('emails_layout', {content: content, annonces: annonces});

      Email.send({
        to:       user.emails[0].address,
        from:     "services@fr.rhenus.com",
        subject:  "Votre réservation",
        html:     html
      });

      console.log("Confirmation de la réservation");


      if (owner){
        var lienProfil = Meteor.absoluteUrl("/"+owner._id)
        var place = Sites.findOne({_id: user.profile.site });

        content = content + `${user.profile.firstname} ${user.profile.lastname} `;

        if (place) {  
          content = content+`( site ${place.name} ) `;
        }

        content = content + ` a réservé ${number} unité(s) de l'annonce suivante : `;
        
        if (annonce.quantity == 0){
          footer = `Votre annonce n'est plus visible car elle ne possède plus d'unité. 
                    Retrouvez vos annonces réservées dans votre profil. ${lienProfil}`;
        }

        var html = SSR.render('emails_layout', {content: content, annonces: annonces, footer:footer});

        Email.send({
          to:       owner.emails[0].address,
          from:     "services@fr.rhenus.com",
          subject:  "Réservation de votre annonce",
          html:     html
        });

        console.log("Alerte de la réservation");
      }
    }
  },
  send_emailPost: function (idAnnonce){
    var annonce = Annonces.findOne({_id:idAnnonce});
    var annonces = [];
    annonces.push( annonce );
    var user = Meteor.users.findOne({_id:annonce.user});
    var content = ""


    if (user){
      var lienProfil = Meteor.absoluteUrl("/"+user._id)
      var content = `Votre annonce ${annonce.title} a bien été posté.`;
      var footer = `Retrouvez vos annonces publiées dans votre profil ${lienProfil}`;
      var html = SSR.render('emails_layout', {content: content, annonces: annonces});

      Email.send({
        to:       user.emails[0].address,
        from:     "services@fr.rhenus.com",
        subject:  "Votre Annonce",
        html:     html
      });

      console.log("Annonce posté");
    }
  }
});