AutoForm.setDefaultTemplate('materialize');
var sendEmail = false
var Annonce = {
    onSuccess: function(formType, result) {
        if (formType === "update") {
            Router.go('userProfile');
        }
        if (formType === "insert") {
            var anc = Annonces.findOne(result);
            if (anc.status === "Publiée") {
                Meteor.call('send_emailPost',result);
            }
            Router.go('listeAnnonces');
        }
    },
    onSubmit: function(insertDoc, updateDoc) {
            if (insertDoc.$set.quantity === 0) {
                insertDoc.$set.status = "Brouillon"
            }
            if (insertDoc.$set.status !== "Brouillon") {
                insertDoc.$set.status = "Publiée";
            }
            return insertDoc;
            if (updateDoc.$set.quantity === 0) {
                updateDoc.$set.status = "Brouillon"
            }
            if (updateDoc.$set.status !== "Brouillon") {
                updateDoc.$set.status = "Publiée";
            }
            return updateDoc;
    }
};

var userEdit = {
    onSuccess: function(formType, result) {
        if (formType === "update") {
            if (Meteor.user().profile.isAdmin) {
                Router.go('usersList');
            } else {
                Router.go('userProfile');
            }
            
        }
    }
};

var siteAdd = {
    onSuccess: function(formType, result) {
        if (formType === "insert" || formType === "update") {
            Router.go('sitesList');
        }
    }
};


var Newsletters = {
    onSuccess: function(formType, result) {
        if (formType === "insert" || formType === "update") {
            Router.go('userProfile');
        }
    }
}



AutoForm.addHooks('Annonce', Annonce);
AutoForm.addHooks('userUpdate', userEdit);
AutoForm.addHooks('AddSites', siteAdd);
AutoForm.addHooks('Newsletters', Newsletters);
