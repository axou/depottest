Template.qualityDelete.events({
	'click .delete': function (event) {
		event.preventDefault();
		var id = this._id;
		sweetAlert({
            title: "Suppression d'un critère d'évaluation",
            text: "La suppression de ce critère d'évaluation est irréversible et"
                    +" entraine des conséquences sur les annonces actuellement en ligne."
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Qualities.remove(id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimé !",
                        text: "Le critère d'évaluation a bien été supprimé",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            });
        });
	}
});
