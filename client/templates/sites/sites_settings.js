Template.siteSetting.events({
	'click .delete': function (event) {
		event.preventDefault();
		var id = this._id;
		sweetAlert({
            title: "Suppression d'un site",
            text: "La suppression de ce site est irréversible et"
                    +" entraine des conséquences sur les annonces et les utilisateurs affiliés à celui-ci."
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Sites.remove(id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimé !",
                        text: "Le site a bien été supprimé, pensez à réattribuer un site aux utilisateurs impactés",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
		        	Router.go("sitesList");
                }
            });
        });
	}
});

