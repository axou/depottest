Template.siteItem.onRendered(function() {  
  GoogleMaps.load({key:"AIzaSyCjBiVbvnmHJkqiG1CcQLOgFsdV1RXwjU0"});
});

Template.siteItem.helpers({
	picture : function() {
		if (this.picture !== undefined) {
			var image =  Images.find(this.picture);
			return image;
		}
	},
	mapOptions: function() {
		if (GoogleMaps.loaded()) {
			return {
				center: new google.maps.LatLng(this.latitude, this.longitude),
				zoom: 12
			};
		}
	}
});

Template.siteItem.onCreated(function() {
	GoogleMaps.ready('map', function(map) {
		var marker = new google.maps.Marker({
			position: map.options.center,
			map: map.instance
		});
	});
});

Template.siteItem.events({
	'click .delete': function (event) {
		event.preventDefault();
		var id = this._id;
		sweetAlert({
            title: "Suppression d'un site",
            text: "La suppression de ce site est irréversible et"
                    +" entraine des conséquences sur les annonces et les utilisateurs affiliés à celui-ci."
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Meteor.call('siteDelete', id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimé !",
                        text: "Le site a bien été supprimé, pensez à réattribuer un site aux utilisateurs impactés",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
		        	Router.go("sitesList");
                }
            });
        });
	}
});