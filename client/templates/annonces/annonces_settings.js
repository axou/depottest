Template.annonceSetting.events({
    'click .delete': function (event) {
        event.preventDefault();
        var id = this._id;
        sweetAlert({
            title: "Suppression d'une annonce",
            text: "La suppression de cette annonce est irréversible "
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Annonces.remove(id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimée !",
                        text: "L'annonce a bien été supprimée",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            });
        });
    }
});
