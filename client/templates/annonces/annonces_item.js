Template.annonce.events({
    'click button': function (e) {
        event.preventDefault();
        userId = Meteor.userId();
        annonce = this._id;
        quantityMax = this.quantity;
        sweetAlert({ 
            title: "Réservation du produit",
            inputPlaceholder: "Quantité à réserver",
            type: "input",
            closeOnConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Réserver',
            cancelButtonText: 'Annuler',
            showLoaderOnConfirm: true,
            allowOutsideClick: false
        }, function(inputValue) {
            var number = parseInt(inputValue)
            if (isNaN(number)) {
                sweetAlert.showInputError("Veuillez saisir un nombre");         
            } else {
                if (number > quantityMax) {
                    sweetAlert.showInputError('La quantité saisie est plus grande que la quantité disponible');
                } else if (number <= 0) {
                    sweetAlert.showInputError('La quantité saisie est nulle');
                } else {
                    Meteor.call("reservation", annonce, userId, number, function(err,response) {
                        if(err) {
                            sweetAlert({
                                type: 'error',
                                title: 'Erreur!',
                                text: "Une erreur est survenue, veuillez réessayer ultérieurement",
                                timer: 1000,
                                showConfirmButton: false
                            });
                        } else {
                            sweetAlert({
                                type: 'success',
                                title: 'Produit réservé !',
                                text: "Votre réservation a bien été prise en compte. L'auteur de l'annonce va prendre contact avec vous.",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    });
                }   
            } 
            
        });
    }
});

Template.annonce.helpers({
    picture: function() {
        var photos = [];
        var size = this.documents.length;
        for (i=0;i<size;i++) {
            id = this.documents[i];
            photos[i] =  Images.findOne(id);
        }
        return photos;
    },
    star: function () {
        var quality = Qualities.findOne(this.rating);
        var qualities = [];
        for (i=0;i<quality.stars;i++) {
            qualities[i] =  i+1;
        }
        return qualities;
    },
    quality: function() {
        var quality = Qualities.findOne(this.rating);
        return quality.label;
    },
    category: function () {
        var ctg = Categories.findOne(this.category);
        return ctg.label;
    },
    reservations: function () {
        var liste = "";
        var size = this.reservedBy.length;
        for (i=0;i<size;i++) {
            id = this.reservedBy[i];
            user =  Meteor.users.findOne(id);
            liste += user.profile.firstname + " " + user.profile.lastname + ".";
        }
        return liste;
    }, 
    color: function () {
        if (this.status === "Publiée") {
            return "blue";
        } else {
            return "orange";
        }
    },
    reserved: function () {
        if (this.user === Meteor.userId() || this.quantity === 0 || Meteor.user().profile.isAdmin) {
            return false;
        } else {
            return true;
        }
    },
    place: function () {
        var where = Sites.findOne(this.site);
        if (where) {
            return where.name;
        } else {
            return "Site non renseigné";
        }
    }
});

Template.annonce.onRendered(function () {
    $('.carousel').carousel({
        full_width: true,
        indicators: true,
        time_constant: 100
    });
    $('.chips').material_chip();
    $('.tooltipped').tooltip({delay: 50});
});
