Template.itemAnnonce.helpers({
    picture: function() {
        if (this.documents !== undefined) { 
            return Images.find(this.documents[0]);
        }
    }, 
    category: function() {
    	var category = Categories.findOne(this.category);
    	if (category !== undefined) {
    		return category.label;
    	} else {
    		return "Autre";
    	}
    },
    color: function () {
        if (this.status === "Publiée") {
            return "blue";
        } else {
            return "orange";
        }
    }
});
