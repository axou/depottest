Template.listeAnnonces.onRendered(function () {
    Session.set('categories', null);
    Session.set('sites', null);
    Session.set('search', null);
	Session.set('limit', 12);
	$('#categories').material_select();
	$('#sites').material_select();
});

Template.listeAnnonces.helpers({
    annonce: function () {
        if (Session.get('categories') !== null) {
            if (Session.get('sites')!== null) {
                if (Session.get('search') !== null) {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        category: {$in : Session.get('categories')}, 
                        site: {$in : Session.get('sites')},
                        title: {$regex : Session.get('search')},
                        description: {$regex : Session.get('search')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                } else {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        category: {$in : Session.get('categories')}, 
                        site: {$in : Session.get('sites')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                }
            } else {
                if (Session.get('search') !== null) {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        category: {$in : Session.get('categories')},
                        title: {$regex : Session.get('search')},
                        description: {$regex : Session.get('search')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                } else {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        category: {$in : Session.get('categories')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                }
            }  
        } else {
            if (Session.get('sites')!== null) {
                if (Session.get('search') !== null) {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        site: {$in : Session.get('sites')},
                        title: {$regex : Session.get('search')},
                        description: {$regex : Session.get('search')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                } else {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}, 
                        site: {$in : Session.get('sites')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                }
            } else {
                if (Session.get('search') !== null) {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]},
                        title: {$regex : Session.get('search')},
                        description: {$regex : Session.get('search')}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                } else {
                    return Annonces.find(
                    {
                        status: {$in: ["Réservée", "Publiée"]}
                    }, 
                    {
                        limit: Session.get('limit')
                    }).fetch();
                }
            }
        }
        
    },
    categories: function () {
    	$('#categories').material_select('update')
    	return Categories.find();
    },
    sites: function () {
    	$('#sites').material_select('update')
    	return Sites.find();
    }
})


Template.listeAnnonces.events({
    'click #more': function (event) {
        event.preventDefault();
        Session.set('limit', Session.get('limit')+12);
    },
    'click #valid': function (event) {
        event.preventDefault();
        var search = $("#search").val();
        var categories = $('#categories').val();
        var sites = $('#sites').val();

        if (search !== "") {
        	Session.set('search', search);
        } else {
            Session.set('search', null);
        }
        if (categories !== null && categories.length > 0) {
        	Session.set('categories', categories);
        } else {
            Session.set('categories', null);
        }
        if (sites !== null && sites.length > 0) {
            console.log(sites.length)
            Session.set('sites', sites);
        } else {
            Session.set('sites', null);
        }
    },
})