Template.annonceAdd.helpers({
	user: function () {
		return Meteor.userId();
	},
	site: function () {
		var user = Meteor.users.findOne(Meteor.userId());
		return user.profile.site;
	}
})
