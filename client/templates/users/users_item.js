Template.userItem.helpers({
	picture: function() {
		if (this.profile.picture !== undefined) {
			var image = Images.find(this.profile.picture);
			return image;
		}
	},
    site: function () {
    	if (this.profile.site !== undefined) {
    		var rslt = Sites.findOne(this.profile.site);
        	return rslt.name + " (" + rslt.address + ", " + rslt.zipCode + " - " + rslt.city + ")";
    	} else {
    		return "Site non renseigné"
    	}
    }
});

Template.userItem.events({
	'click .delete': function (event) {
		event.preventDefault();
		var id = this._id;
		sweetAlert({
            title: "Suppression d'un utilisateur",
            text: "Les annonces en ligne de cet utilisateur "
                    +" seront attribué à l'administrateur du site."
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Meteor.call('usersDelete', id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimé !",
                        text: "L'utilisateur a bien été supprimé, ses annonces ont été attribuée",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
		        	Router.go("usersList");
                }
            });
        });
	}
});
