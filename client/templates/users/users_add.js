Template.userAdd.events({
    "submit form": function(event, template) {
		event.preventDefault();
		var file = $('#photoUser').get(0).files[0]
		var lastname = $("#lastname").val();
		var firstname = $("#firstname").val();
		var email = $("#email").val();
		var idSite = $("#site").val();
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		var isAdmin = ($("#isAdmin").attr("checked")=="checked") ? true : false;

		if( lastname!="" && firstname!="" && email!="" && idSite!="" && password!="" && repassword!="" ){
			
			if(password == repassword){
				fsFile = new FS.File(file);
				var pictureUser = Images.insert(fsFile,function(err,succes){
					if(err) {
						console.log(err.reason);
					}
			    });
				var user = {
					username: firstname+'.'+lastname,
					email: email,
					password: password,
					profile:{
						firstname: firstname,
						lastname: lastname,
						isAdmin: isAdmin,
						site: idSite,
						picture: pictureUser._id
					}
				};
				Meteor.call("userAdd", user, function (err, result) {
					if (err) {
						alert(err.reason)
					}else{
						Router.go('/Users');
					};
				});
			}else{
				sweetAlert({
                    title: "Mot de passe incorrect !",
                    text: "Les mots de passe saisis ne sont pas identiques",
                    type: "error",
                    timer: 1500,
                    showConfirmButton: false
                });
			}
		}else{
			sweetAlert({
                title: "Formulaire incomplet !",
                text: "Veuillez remplir tous les champs obligatoires",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
		}
	}
});

Template.userAdd.helpers({
	allSites: function () {
		return Sites.find({});
	}
});


Template.userAdd.onRendered(function () {
	$(document).ready(function() {
    	$('select').material_select();
  	});
})
