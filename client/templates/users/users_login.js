Template.login.events({
    "submit form": function(event, template) {
		event.preventDefault();

		var username = $("#username").val();
		var password = $("#password").val();

		Meteor.loginWithPassword({
			username: username
		}, password, function(err) {
			if (err) {
				sweetAlert({
                    title: "Erreur !",
                    text: "Les informations fournies sont incorrectes",
                    type: "error",
                    timer: 1000,
                    showConfirmButton: false
                });
			} else {
				Router.go('home');
			}
		});
	}
});