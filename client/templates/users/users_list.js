Template.usersList.helpers({
	users: function () {
		return Meteor.users.find({}, {sort: {"profile.lastname": 1, "profile.firstname": 1}}).fetch();
	}
})