Template.userProfile.helpers({
    email: function () {
        me = Meteor.user();
        return me && me.emails && me.emails[0].address
    },
    picture: function () {
        me = Meteor.user();
        if (me.profile.picture !== undefined) {
            return Images.find(me.profile.picture);
        }
    },
    place: function () {
        me = Meteor.user();
        site = Sites.findOne(me.profile.site);
        return site.name + " (" + site.address + " - " + site.zipCode + " " + site.city + ")";
    },
    me: function () {
        return {user: Meteor.userId()};
    }
});
