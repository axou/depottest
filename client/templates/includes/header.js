Template.header.events({
    "click .logout": function(event, template) {
		sweetAlert({
            title: "Deconnexion",
            text: "Êtes-vous sûr de vouloir vous déconecter ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: true
        }, function () {
			Meteor.logout();
        });
	}
});


Template.header.helpers({
	admin : function(){
		return currentUser.profile.isAdmin;
	}
});


Template.header.onRendered(function () {
	$(".button-collapse").sideNav();
})