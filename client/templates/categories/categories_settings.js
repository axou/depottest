Template.categoryDelete.events({
	'click .delete': function (event) {
		event.preventDefault();
		var id = this._id;
		sweetAlert({
            title: "Suppression d'une catégorie",
            text: "La suppression de cette catégorie est irréversible et"
                    +" entraine des conséquences sur les annonces actuellement en ligne."
                    +" Êtes-vous sûr de vouloir continuer ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            closeOnConfirm: false
        }, function () {
            Meteor.call('deleteCategories', id, function(error, result) {
                if (error) {
                    sweetAlert("Erreur", error.reason, "error");
                } else {
                    sweetAlert({
                        title: "Supprimée !",
                        text: "La catégorie a bien été supprimée",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            });
        });
	}
});