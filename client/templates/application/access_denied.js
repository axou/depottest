Template.accessDenied.onRendered(function () {
	sweetAlert({
        title: "Accès interdit !",
        text: "L'accès à cette page vous a été refusé. Vous allez être redirigé vers l'acceuil",
        type: "error",
        timer: 2000,
        showConfirmButton: false
    });
	setTimeout(function(){ 
		Router.go('home');
	}, 2000)
})