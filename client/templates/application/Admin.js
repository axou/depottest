Template.Admin.onRendered(function() {  
    function valCategories() {
        var aCategories = [];
        var cpt = 0;
        Categories.find().forEach(function (category) {
            console.log(category.label)
            aCategories[cpt] = { name: category.label, y: Annonces.find({"category": category._id, "status": "Publiée"}).count() };
            cpt++;
        });
        return aCategories;
    }
    function valSites() {
    	var aSites = [];
    	var cpt = 0;
    	Sites.find().forEach(function (site) {
    		aSites[cpt] = { name: site.name, y: Annonces.find({"site": site._id, "status": "Publiée"}).count() };
    		cpt++;
    	});
        return aSites;
    }
    function valMonths() {
    	var array = [];
    	var year = new Date().getFullYear();
    	var months = ["01","02","03","04","05","06","07","08","09","10","11","12"];
    	months.forEach(function (month) {
    		dateCheck = month+"-"+year;
    		array.push(Annonces.find({"publication": {'$regex': dateCheck}, "status": "Publiée"}).count());
    	});
    	var aMonths = [{name: "Annonces", data: array}];
        return aMonths;
    }


	$('#graph1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            borderColor: "#00469B",
            borderWidth: 1
        },
        colors: ['#6e6e6e', '#fabb00', '#00469B', '#cfcfd1', '#f6dd96', '#cce0ee'],
        title: {
            text: 'Répartition des annonces publiées par catégories',
            style: {
                fontSize: '24px'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: "Nombre d'annonces publiées",
            colorByPoint: true,
            data: valCategories()
        }]
    });

    $('#graph2').highcharts({
        chart: {
            type: 'column',
            borderColor: "#00469B",
            borderWidth: 1
        },
        title: {
            text: "Nombre d'annonces publiées par sites",
            style: {
                fontSize: '24px'
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: "Nombre d'annonces publiées: <b>{point.y}</b>"
        },
        series: [{
            name: 'Annonces publiées',
            data: valSites(),
            dataLabels: {
                enabled: true,
                verticalAlign: "middle",
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y}',
                y: 10,
                style: {
                    fontSize: '16px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('#graph3').highcharts({
    	chart: {
            borderColor: "#00469B",
            borderWidth: 1
    	},
        title: {
            text: "Nombre d'annonces publiées par mois en "+new Date().getFullYear(),
            x: -20,
            style: {
                fontSize: '24px'
            }
        },
        xAxis: {
            categories: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: "Nombre d'annonces publiées"
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            pointFormat: "Nombre d'annonces publiées: <b>{point.y}</b>"
        },
        legend: {
            enabled: false
        },
        series: valMonths()
    });

});