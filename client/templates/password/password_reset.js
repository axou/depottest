Template.resetPassword.helpers({
	resetPassword: function() {
		var valid = false;
		var token = Router.current().params.token;
		var user = Meteor.users.findOne({ "services.password.reset.token": token });
		
		if (user) {
			var diff=0;
			var dateToken = new Date(user.services.password.reset.when);
			var now = new Date();

			diff = Math.floor(now-dateToken);
			//différence en min
			diff = Math.floor(diff/1000/60);

			if (diff < 60) {
				valid = true
			}
		}
		
		return valid;
	}
});

Template.resetPassword.events({
	'click .submit': function (){
		var newPassword = $("#newPassword").val();
		var confirmPassword = $("#confirmPassword").val();
		var token = Router.current().params.token;

		if ( newPassword = confirmPassword ) {
			Accounts.resetPassword(token, newPassword, function (err) {
				if (err) {
					alert(err);
				} else {
					console.log("Mot de passe modifié");
					Router.go("/");
			    }
			});
		}
	}
});