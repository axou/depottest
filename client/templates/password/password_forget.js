Template.forgetPassword.events({
	"submit form" : function(event, template){
		var email =  $("#forgotPasswordEmail").val();

		Meteor.call("send_passwordLink",email, function(error, result) {
	        if (error) {
	            return console.log(error.reason);
	        }
	    });

	}
});