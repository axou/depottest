Sites = new Mongo.Collection('sites');



Sites.helpers({
	fullAdress: function () {
		return this.address + ', ' + this.zipCode + ' ' + this.city;
	},
	viewPicture: function () {
		return this.picture.file.url;
	}
});

Sites.allow({
    insert: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    update: function (userId, doc, fields, modifier) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    remove: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    }
});