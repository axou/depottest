SMTP = new Mongo.Collection('smtp');

Meteor.methods({
	smtpSetting: function(smtp) {
		check(smtp, SchemaSMTP);

		encryptPwd = CryptoJS.AES.encrypt( smtp.password, "").toString();
		smtp.password = encryptPwd;

		smtpExist = SMTP.findOne({});
		
		if (smtpExist) {
			SMTP.remove({});
			SMTP.insert(smtp);
        	console.log("SMTP reconfiguré");
		}else{
			SMTP.insert(smtp);
        	console.log("SMTP configuré");
		}
	}
});
