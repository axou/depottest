Qualities = new Mongo.Collection('qualities');

Qualities.allow({
    insert: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    remove: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    }
});
