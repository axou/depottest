Categories = new Mongo.Collection("categories");

Meteor.methods({
	insertCategories: function (categorie) {
        var id = Categories.insert(categorie);
		Newsletters.find().forEach(function (news) {
            Newsletters.update(news._id, {$push: {favoriteCategories: id}}, function (err){
            	console.log(err);
            });
        });
	},
	deleteCategories: function (id) {
		Newsletters.find().forEach(function (news) {
            Newsletters.update(news._id, {$pull: {favoriteCategories: id}}, function (err){
            	console.log(err);
            });
        });
        Categories.remove(id);
	}
});

Categories.allow({
    insert: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    update: function (userId, doc, fields, modifier) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    remove: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    }
});