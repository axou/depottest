// COLLECTION METEOR.USERS

//Méthodes Meteor
Meteor.methods({
    userUpdate: function (user, id) {
        user.$set.username = user.$set['profile.firstname'] + '.' + user.$set['profile.lastname'];

        //console.log(user);
        // check(user, Schemas.users);

        Meteor.users.update(id, user, function(err) {
            if (err) {
                alert(err);
            }
            Router.go('usersList');
            // Router.go('/Users');
        });
    },
    userAdd: function (user) {
        var id = Accounts.createUser(user);
        var categories = [];
        Categories.find().forEach(function (category) {
            categories.push(category._id);
        });
        if (!user.profile.isAdmin) {
            Newsletters.insert({
                idUser: id,
                emailUser: user.email,
                frequency: 'daily',
                favoriteCategories: categories
            });
        }
    },
    usersDelete: function (id) {
        Annonces.find({user: id}).forEach(function (annonce) {
            Annonces.update(annonce._id, {$set: {user: Meteor.userId()}});
        });
        Newsletters.find({idUser: id}).forEach(function (news) {
            Newsletters.remove(news._id);
        });
        Meteor.users.remove(id);
    }

});

Meteor.users.allow({
    insert: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    },
    update: function (userId, doc, fields, modifier) {
        if (Meteor.users.findOne(userId).profile.isAdmin || doc._id === userId) {
            return true;
        } else {
            return false;
        }
    },
    remove: function (userId, doc) {
        return Meteor.users.findOne(userId).profile.isAdmin;
    }
});
