// COLLECTION ANNONCES

// Création d'une collection Annonces
Annonces = new Mongo.Collection('annonces');

// Autorisation de la collection Annonces
Annonces.allow({
    insert: function (userId, doc) {
        if (Meteor.users.findOne(userId).profile.isAdmin) {
            return false;
        } else {
            return true;
        }
    },
    update: function (userId, doc, fields, modifier) {
        return true;
    },
    remove: function (userId, doc) {
        return true;
    }
});

// Méthodes Meteor pour la collection Annonces
Meteor.methods({
    reservation: function (id, userId, number) {
        check(id, String);
        check(userId, String);
        check(number, Number);
        annonce = Annonces.findOne(id);
        qty = parseInt(annonce.quantity - number);
        if (Annonces.findOne({_id: id, 'reservedBy': userId})) {
            if (qty === 0) {
                Annonces.update(id, {$set: {quantity: qty, status: "Réservée"}});
                Meteor.call("send_emailReservation", id, userId, number);
            } else {
                Annonces.update(id, {$set: {quantity: qty}}, function(err,response) {
                    if(err) {
                        console.log(err);
                    } else {
                        Meteor.call("send_emailReservation", id, userId, number);
                        console.log(response);
                    }
                });
            }
        } else {
            if (qty === 0) {
                Annonces.update(id, {$set: {quantity: qty, status: "Réservée"}, $push:{reservedBy: userId}});
                Meteor.call("send_emailReservation", id, userId, number);
            } else {
                Annonces.update(id, {$set: {quantity: qty}, $push:{reservedBy: userId}}, function(err,response) {
                    if(err) {
                        console.log(err);
                    } else {
                        console.log(response);
                        Meteor.call("send_emailReservation", id, userId, number);
                    }
                });
            }
        }
        
    }
});

Annonces.helpers({
    quality: function () {
        var qualite = Qualities.findOne(this.rating);
        return qualite.label;
    },
    place: function () {
        var place = Sites.findOne(this.site);
        return place.name + " (" + place.address + " - " + place.zipCode + " " + place.city + ")";
    },
    photos: function () {
        return this.documents.length;
    },
    categorie: function() {
        var category = Categories.findOne(this.category);
        return category.label;
    }
});
