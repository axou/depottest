schemaSites = new SimpleSchema({
	name: {
		type: String,
		label: 'Nom du site',
		min: 5,
		max: 25,
		unique: true
	},
	address: {
		type: String,
		label: 'Numéro et nom de rue',
		min: 5,
		max: 50
	},
	zipCode: {
		type: String,
		label: 'Code postale',
		regEx: SimpleSchema.RegEx.ZipCode,
		min: 5,
		max: 5
	},
	city: {
		type: String,
		label: 'Ville',
		min: 3,
		max: 30
	},
	longitude: {
		type: Number,
		label: 'Longitude',
		decimal: true
	},
	latitude: {
		type: Number,
		label: 'Latitude',
		decimal: true
	},
	picture: {
		type: String,
		label: 'Photo',
		optional: true,
		autoform: {
			afFieldInput: {
				type: 'fileUpload',
				collection: 'Images',
				accept: 'image/*',
				label: 'Ajouter une photo',
				previewTemplate: 'pictureView'
			}
		}
	}
});

Sites.attachSchema(schemaSites);

