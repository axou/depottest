schemaCategories = new SimpleSchema({
	label: {
		type: String,
		label: 'Categorie',
        unique: true,
		min: 2,
		max: 25
	}
});

Categories.attachSchema(schemaCategories);
