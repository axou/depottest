SchemaNewsletter = new SimpleSchema ({
	idUser: {
        type: String,
        autoform: {
            type: 'hidden'
        }
    },
	emailUser: {
		type: String,
		label: 'Email',
        regEx: SimpleSchema.RegEx.Email
	},
    frequency: {
    	type: String,
    	label: 'Fréquence',
        allowedValues: ['daily', 'monthly', 'weekly'],
        autoform: {
            type: 'select-radio-inline',
            options: function () {
                return [
                    {label: "Journalière", value: "daily"},
                    {label: "Hebdomadaire", value: "weekly"},
                    {label: "Mensuelle", value: "monthly"},
                ];
            },
            firstOption: false
        }
    },
	favoriteCategories: {
		type: [String],
        label: 'Catégories',
        autoform: {
            type: 'select-checkbox-inline',
            options: function()
            {
                return Categories.find().map(function (doc) {
                    return {label: doc.label, value: doc._id};
                });
            }
        }
	},
    lastSend: {
        type: String,
        label: 'Dernier envoi',
        optional: true,
        autoValue: function () {
            'use strict';
            if (this.isInsert) {
                return moment().format('DD-MM-YYYY');
            }
        }
    },
});

Newsletters.attachSchema(SchemaNewsletter);