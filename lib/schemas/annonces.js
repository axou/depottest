// SCHEMA ANNONCES

// Création du schema des documents de la collection Annonces
schemasAnnonces = new SimpleSchema({
    user: {
        type: String,
        autoform: {
            type: 'hidden'
        }
    },
    site: {
        type: String,
        label: 'Site',
        autoform: {
            type: 'select',
            options: function()
            {
                return Sites.find().map(function (doc) {
                    return {label: doc.name, value: doc._id};
               });
            },
            firstOption: false
        }
    },
    title: {
        type: String,
        label: "Titre de l'annonce"
    },
    description: {
        type: String,
        label: 'Description du produit',
        autoform: {
            type: 'textarea'
        }
    },
    category: {
        type: String,
        label: 'Catégorie du produit',
        autoform: {
            type: 'select',
            options: function()
            {
                return Categories.find().map(function (doc) {
                    return {value: doc._id, label: doc.label};
                });
            },
            firstOption: 'Catégorie'
        }
    },
    quantity: {
        type: Number,
        label: "Nombre d'unité mise à disposition",
        min: 0,
        decimal: false
    },
    rating: {
        type: String,
        label: 'Qualité du produit',
        autoform: {
            type: 'select',
            options: function()
            {
                return Qualities.find().map(function (doc) {
                    return {value: doc._id, label: doc.label};
                });
            },
            firstOption: 'Qualité'
        }
    },
    documents: {
        type: Array,
        label: "Photos et autre document",
        optional: true,
        maxCount: 3
    },
    "documents.$": {
        type: String,
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images',
                label: 'Ajouter une photo',
                previewTemplate: 'pictureView'
            }
        }
    },
    status: {
        type: String,
        allowedValues: ['Brouillon', 'Publiée', 'Réservée', 'Périmée'],
        autoform: {
            options: [
                {label: "Brouillon", value: "Brouillon"},
                {label: "Publiée", value: "Publiée"}
            ],
            firstOption : false
        }
    },
    publication: {
        type: String,
        label: 'Date de publication',
        optional: true,
        autoValue: function () {
            'use strict';
            return moment().format('DD-MM-YYYY');
        }
    },
    deadline: {
        type: String,
        label: 'Date de fin de publication',
        optional: true,
        autoValue: function () {
            'use strict';
            return moment().add(3, 'M').format('DD-MM-YYYY');
        }
    },
    reservedBy: {
        type: [String],
        label: 'Réservée par ',
        optional: true
    }
});

Annonces.attachSchema(schemasAnnonces);
