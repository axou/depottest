SchemaQualities = new SimpleSchema ({
	stars: {
		type: Number,
		label: "Nombre d'étoiles",
        unique: true
	},
	label: {
		type: String,
		label: 'Qualité',
        unique: true
    }
});


Qualities.attachSchema(SchemaQualities);
