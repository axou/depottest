// SCHEMA USERS

// Initialisation de la variable Schemas
// Tableau JSON
var Schemas = {};

// Création du schema du profile des utilisateurs
Schemas.UserProfile = new SimpleSchema({
    firstname: {
        type: String,
        label: 'Prénom'
    },
    lastname: {
        type: String,
        label: 'Nom'
    },
    isAdmin: {
        type: Boolean,
        label: 'Administrateur'
    },
    site: {
        type: String,
        label: "Site",
        autoform: {
            type: 'select',
            options: function () {
                'use strict';
                var values = Sites.find().map(function (post) {
                    return {label: post.name, value: post._id};
                });
                return values;
            },
            firstOption: false,
            noselect: true
        }
    },
    picture: {
        type: String,
        label: 'Photo de profil',
        optional: true,
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images',
                accept: 'image/*',
                label: 'Ajouter une photo',
                previewTemplate: 'pictureView'
            }
        }
    }
});


// Création du schema des documents de la collection Meteor.users
Schemas.users = new SimpleSchema({
    username: {
        type: String
    },
    emails: {
        type: Array,
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        label: "Email",
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    registered_emails: {
        type: [Object],
        optional: true,
        blackbox: true
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Schemas.UserProfile,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    heartbeat: {
        type: Date,
        optional: true
    }
});


// Jointure entre le schema et la collection
Meteor.users.attachSchema(Schemas.users, {transform: true});
