SchemaSMTP = new SimpleSchema ({
	email: {
		type: String,
		label: "Email",
        regEx: SimpleSchema.RegEx.Email
	},
	password: {
		type: String,
		label: "Mot de passe"
	},
	smtp: {
		type: String,
		label: "SMTP"
	},
	port: {
		type: Number,
		label: "Port"
	}
});

SMTP.attachSchema(SchemaSMTP);