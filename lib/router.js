Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: 'loading',
	notFoundTemplate: 'notFound'
});

Router.onBeforeAction(function() {
	if(! Meteor.userId()){
		if (Router.current().route.getName() == 'forgetPassword') {
			this.render('forgetPassword');
		} else if(Router.current().route.getName() == 'resetPassword'){
			this.render('resetPassword');
		} else {
			this.render('login');
		}
	}else{
		this.next();
	}
});

Router.route('/', {
	name: 'home',
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
			Meteor.subscribe('users'),
		];
	}
});

/* MODULE UTILISATEURS */
Router.route('/Users', {
	name: 'usersList',
	waitOn: function () {
		return [
			Meteor.subscribe('sites'), Meteor.subscribe('users')
		];
	}
});
Router.route('/addUser', {
	name:'userAdd',
	waitOn: function () {
		return [
			Meteor.subscribe('sites')
		];
	}
});
Router.route('/Users/:_id/edit', {
	name: 'userUpdate',
	data: function () {
		return Meteor.users.findOne(this.params._id);
	},
	waitOn: function () {
		return [
			Meteor.subscribe('sites'),
			Meteor.subscribe('users')
		];
	}
});
Router.route('/profil', {
	name: 'userProfile',
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
			Meteor.subscribe('users'),
		];
	}
});

/* MODULE CATEGORIES */
Router.route('/Categories', {
	name: 'categoriesList',
	waitOn: function () {
		return [
			Meteor.subscribe('categories')
		];
	}
});

/* MODULE QUALITES */
Router.route('/Qualities', {
	name: 'qualitiesList',
	waitOn: function () {
		return [
			Meteor.subscribe('qualities')
		];
	}
});

/* MODULE SITES */
Router.route('/Sites', {
	name: 'sitesList',
	waitOn: function () {
		return [
			Meteor.subscribe('sites')
		];
	}
});
Router.route('/SiteAdd', {name: 'siteAdd'});
Router.route('/Site/:_id', {
	name: 'siteItem',
	data: function () {
		return Sites.findOne(this.params._id);
	},
	waitOn: function () {
		return [
			Meteor.subscribe('sites'),
		];
	}
});
Router.route('/Site/:_id/edit', {
	name: 'siteEdit',
	data: function () {
		return Sites.findOne(this.params._id);
	},
	waitOn: function () {
		return [
			Meteor.subscribe('sites'),
		];
	}
});


/* MODULE STATISTIQUES */
Router.route('/Statistics', {
	name: 'Admin',
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
			Meteor.subscribe('users'),
		];
	}
});


/* MODULE ANNONCES */
Router.route('/Annonce/add', {
	name: 'AnnonceAdd',
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
		];
	}
});
Router.route('/Annonce/:_id/edit', {
	name: 'annonceEdit',
	data: function () {
		return Annonces.findOne(this.params._id);
	},
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
		];
	}
});
Router.route('/Annonces/', {
	name: 'listeAnnonces',
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
		];
	}
});
Router.route('/Annonce/:_id', {
	name: 'annonce',
	data: function() {
		return Annonces.findOne(this.params._id);
	},
	waitOn: function () {
		return [
			Meteor.subscribe('annonces'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('sites'),
			Meteor.subscribe('qualities'),
			Meteor.subscribe('users'),
		];
	}
});


/* MODULE PASSWORD */
Router.route('/forgetPassword',{
	name: 'forgetPassword'
});
Router.route('/changePassword',{
	name: 'changePassword'
});
Router.route('/resetPassword/:token',{
	name: 'resetPassword',
	waitOn: function () {
		return [
			Meteor.subscribe('users')
		];
	}
});

/* MODULE NEWSLETTERS */
Router.route('newsletters',{
	name: 'newslettersEdit',
	waitOn: function () {
		return [
			Meteor.subscribe('newsletters'),
			Meteor.subscribe('categories'),
			Meteor.subscribe('users')
		];
	}
});

// CONTROLS
// Control Administrateur
var adminControl = function () {
    'use strict';
    var user = Meteor.user();
    if (user === null) {
        this.redirect('login');
    } else {
        if (user.profile.isAdmin === false) {
            this.render('accessDenied');
        } else {
            this.next();
        }
    }
}

var userControl = function () {
    'use strict';
    var user = Meteor.user();
    if (user === null) {
        this.redirect('login');
    } else {
        if (user.profile.isAdmin) {
            this.render('accessDenied');
        } else {
            this.next();
        }
    }
}


Router.onBeforeAction(adminControl, {only: ['usersList','userAdd','categoriesList','addCategory', 'qualitiesList', 'addQuality', 'sitesList', 'siteAdd', 'siteEdit', 'Admin']});
Router.onBeforeAction(userControl, {only: ['AnnonceAdd']});
