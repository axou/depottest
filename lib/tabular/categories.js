TabularTables = {};

TabularTables.Categories = new Tabular.Table({
	name: 'Categories',
	collection: Categories,
	columns: [
		{data:'label', title:'Catégorie'},
		{
			title: 'Suppression',
			tmpl: Meteor.isClient && Template.categoryDelete
		}
	],
	language: {
		"info":           "Afficher _START_ à _END_ sur _TOTAL_ catégories",
		"infoEmpty":      "Afficher 0 à 0 sur 0 catégories",
		"lengthMenu":     "Afficher _MENU_ catégories",
		"loadingRecords": "Chargement...",
		"processing":     "En traitement...",
		"search":         "Rechercher:",
		"zeroRecords":    "Aucune catégorie correspondante trouvée",
		"paginate": {
			"first":      "Première",
			"last":       "Dernière",
			"next":       "Suivante",
			"previous":   "Précédente"
		},
	}
});