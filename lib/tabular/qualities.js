TabularTables = {};

TabularTables.Qualities = new Tabular.Table({
	name: 'Qualities',
	collection: Qualities,
	responsive: true,
	autoWidth: false,
	columns: [
		{data:'label', title:'Qualité'},
		{data:'stars', title:'Etoiles'},
		{
			title: 'Suppression',
			tmpl: Meteor.isClient && Template.qualityDelete
		}
	],
	language: {
		"info":           "Afficher _START_ à _END_ sur _TOTAL_ qualités",
		"infoEmpty":      "Afficher 0 à 0 sur 0 qualités",
		"lengthMenu":     "Afficher _MENU_ qualités",
		"loadingRecords": "Chargement...",
		"processing":     "En traitement...",
		"search":         "Rechercher:",
		"zeroRecords":    "Aucune qualité correspondante trouvée",
		"paginate": {
			"first":      "Première",
			"last":       "Dernière",
			"next":       "Suivante",
			"previous":   "Précédente"
		},
	}
});
