TabularTables.Sites = new Tabular.Table({
	name: 'Sites',
	collection: Sites,
	responsive: true,
	autoWidth: false,
	extraFields: ['address', 'zipCode', 'city', 'picture'],
	columns: [
		{data:'name', title:'Site'},
		{data:'fullAdress()', title:'Adresse'},
		{
			title: 'Actions',
			tmpl: Meteor.isClient && Template.siteSetting
		}
	],
	language: {
		"info":           "Afficher _START_ à _END_ sur _TOTAL_ sites",
		"infoEmpty":      "Afficher 0 à 0 sur 0 sites",
		"lengthMenu":     "Afficher _MENU_ sites",
		"loadingRecords": "Chargement...",
		"processing":     "En traitement...",
		"search":         "Rechercher:",
		"zeroRecords":    "Aucun site correspondant trouvé",
		"paginate": {
			"first":      "Premier",
			"last":       "Dernier",
			"next":       "Suivant",
			"previous":   "Précédent"
		},
	}
});