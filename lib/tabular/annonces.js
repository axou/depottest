TabularTables = {};

TabularTables.Annonces = new Tabular.Table({
    name: 'Annonces',
    collection: Annonces,
    /*selector: function () {
        return {user: this.userId};
    },*/
    responsive: true,
    autoWidth: false,
    extraFields: ['rating', 'documents', 'site', 'category'],
    columns: [
        {data:'title', title:'Titre'},
        {data:'categorie()', title:'Catégorie'},
        {data: 'description', title:"Description"},
        {data: 'quality()', title:"Qualité"},
        {data: 'quantity', title:"Quantité"},
        {data: 'place()', title:"Site"},
        {data: 'status', title:"Status"},
        {title: 'Actions',tmpl: Meteor.isClient && Template.annonceSetting}
    ],
    language: {
        "info":           "Afficher _START_ à _END_ sur _TOTAL_ annonces",
        "infoEmpty":      "Afficher 0 à 0 sur 0 annonce",
        "lengthMenu":     "Afficher _MENU_ annonces",
        "loadingRecords": "Chargement...",
        "processing":     "En traitement...",
        "search":         "Rechercher:",
        "zeroRecords":    "Aucun correspondance trouvée",
        "paginate": {
            "first":      "Première page",
            "last":       "Dernière page",
            "next":       "Suivant",
            "previous":   "Précédent"
        },
    }
});
